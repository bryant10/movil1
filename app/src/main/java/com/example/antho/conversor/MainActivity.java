
package com.example.antho.conversor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText centi,faren, CEN,FA;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Toast.makeText(this, "Lopez Pachay Bryant Anthony", Toast.LENGTH_SHORT).show();
        Log.w("MainActivity", "Lopez Pachay Bryant Anthony");


        setContentView(R.layout.activity_main);
        this.centi=(EditText)findViewById(R.id.txtCentigrados);
        this.faren=(EditText)findViewById(R.id.txtFarenheit);

//  EVENTO "Aqui lo que hacemos es despues de escribir la cantidad damos enter y se realiza la conversion"

        this.centi.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //convirtiendo
                float fr;

                fr=(1.8f)*Float.parseFloat(centi.getText().toString())+32;

                faren.setText(""+fr);

                return false;
            }
        });

        this.CEN=(EditText)findViewById(R.id.txtC);
        this.FA=(EditText)findViewById(R.id.txtF);
        //EVENTO 2

        this.FA.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //convirtiendo2
                float  CT;
                CT=(Float.parseFloat(FA.getText().toString())-32)/(1.8f);
                CEN.setText(""+CT);
                return false;
            }
        });





    }}
